package br.com.ponto.repositories;

import br.com.ponto.models.Registro;
import br.com.ponto.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface RegistroRepository extends CrudRepository<Registro, Integer> {

    Iterable<Registro> findByUsuario(Usuario usuario);

    Registro findTop1ByUsuarioOrderByIdDesc(Usuario usuario);
}
