package br.com.ponto.controllers;

import br.com.ponto.models.Usuario;
import br.com.ponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    public ResponseEntity<?> inserirUsuario(@RequestBody @Valid Usuario usuario) {
        try {
            Usuario objetoUsuario = usuarioService.inserirUsuario(usuario);
            return ResponseEntity.status(HttpStatus.CREATED).body(objetoUsuario);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<?> selecionarTodosUsuarios() {
        try {
            Iterable<Usuario> usuarios = usuarioService.selecionarTodosUsuarios();
            return ResponseEntity.status(HttpStatus.OK).body(usuarios);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> selecionarUsuarioPorId(@PathVariable Integer id) {
        try {
            Usuario usuario = usuarioService.selecionarUsuarioPorId(id);
            return ResponseEntity.status(HttpStatus.OK).body(usuario);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> atualizarUsuario(@PathVariable Integer id, @RequestBody Usuario usuario) {
        try {
            Usuario objetoUsuario = usuarioService.atualizarUsuario(id, usuario);
            return ResponseEntity.status(HttpStatus.OK).body(objetoUsuario);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarUsuario(@PathVariable Integer id) {
        try {
            usuarioService.deletarUsuario(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
