package br.com.ponto.controllers;

import br.com.ponto.dtos.RegistroDetalheDto;
import br.com.ponto.enums.TipoRegistro;
import br.com.ponto.models.Registro;
import br.com.ponto.services.RegistroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/registros")
public class RegistroController {

    @Autowired
    private RegistroService registroService;

    @PostMapping("/usuario/{idUsuario}")
    public ResponseEntity<?> registrarPonto(@PathVariable Integer idUsuario, @RequestParam(name = "tipoRegistro", required = true) TipoRegistro tipoRegistro) {
        try {
            Registro registro = registroService.registrarPonto(idUsuario, tipoRegistro);
            return ResponseEntity.status(HttpStatus.CREATED).body(registro);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/usuario/{idUsuario}")
    public ResponseEntity<?> selecionarRegistrosPorUsuario(@PathVariable Integer idUsuario) {
        try {
            RegistroDetalheDto registroDetalheDto = registroService.selecionarRegistrosPorUsuario(idUsuario);
            return ResponseEntity.status(HttpStatus.OK).body(registroDetalheDto);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
