package br.com.ponto.dtos;

import br.com.ponto.models.Registro;

import java.math.BigDecimal;

public class RegistroDetalheDto {

    private Iterable<Registro> registros;

    private BigDecimal quantidadeHorasTrabalhadas;

    public RegistroDetalheDto() {
    }

    public Iterable<Registro> getRegistros() {
        return registros;
    }

    public void setRegistros(Iterable<Registro> registros) {
        this.registros = registros;
    }

    public BigDecimal getQuantidadeHorasTrabalhadas() {
        return quantidadeHorasTrabalhadas;
    }

    public void setQuantidadeHorasTrabalhadas(BigDecimal quantidadeHorasTrabalhadas) {
        this.quantidadeHorasTrabalhadas = quantidadeHorasTrabalhadas;
    }
}
