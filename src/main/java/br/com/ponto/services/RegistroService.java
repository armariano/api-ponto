package br.com.ponto.services;

import br.com.ponto.dtos.RegistroDetalheDto;
import br.com.ponto.enums.TipoRegistro;
import br.com.ponto.models.Registro;
import br.com.ponto.models.Usuario;
import br.com.ponto.repositories.RegistroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class RegistroService {

    @Autowired
    RegistroRepository registroRepository;

    @Autowired
    UsuarioService usuarioService;

    public Registro registrarPonto(Integer idUsuario, TipoRegistro tipoRegistro) {

        Usuario usuario = usuarioService.selecionarUsuarioPorId(idUsuario);

        Registro ultimoRegistro = registroRepository.findTop1ByUsuarioOrderByIdDesc(usuario);

        if(ultimoRegistro != null){
            if (tipoRegistro.equals(ultimoRegistro.getTipoRegistro())) {
                throw new RuntimeException("Tipo de registro de entrada é igual ao atual");
            }
        }

        Registro registro = new Registro();
        registro.setUsuario(usuario);
        registro.setTipoRegistro(tipoRegistro);
        registro.setDataHoraRegistro(LocalDateTime.now());

        if (tipoRegistro.equals(TipoRegistro.SAIDA)) {
            Duration duration = Duration.between(ultimoRegistro.getDataHoraRegistro(), registro.getDataHoraRegistro());

            Long segundos = Math.abs(duration.getSeconds());
            Long minutos = segundos / 60;

            registro.setMinutosTrabalhados(minutos);
        } else {
            registro.setMinutosTrabalhados(0L);
        }

        registro = registroRepository.save(registro);

        return registro;
    }

    public RegistroDetalheDto selecionarRegistrosPorUsuario(Integer idUsuario) {

        RegistroDetalheDto registroDetalheDto = new RegistroDetalheDto();


        Usuario usuario = usuarioService.selecionarUsuarioPorId(idUsuario);

        Iterable<Registro> registros = registroRepository.findByUsuario(usuario);

        List<Registro> novaListaRegistros = (List) registros;

        Long minutosTrabalhados = novaListaRegistros.stream().mapToLong(x -> x.getMinutosTrabalhados()).sum();

        BigDecimal horasTrabalhadas = BigDecimal.valueOf(minutosTrabalhados / 60);

        registroDetalheDto.setRegistros(registros);
        registroDetalheDto.setQuantidadeHorasTrabalhadas(horasTrabalhadas);

        return registroDetalheDto;


    }
}
