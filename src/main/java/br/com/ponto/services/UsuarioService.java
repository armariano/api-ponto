package br.com.ponto.services;

import br.com.ponto.models.Usuario;
import br.com.ponto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Iterable<Usuario> selecionarTodosUsuarios(){
        Iterable<Usuario> usuarios = usuarioRepository.findAll();
        return usuarios;
    }

    public Usuario selecionarUsuarioPorId(int id){
        Usuario usuario = null;
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
        if (optionalUsuario.isPresent()){
            usuario = optionalUsuario.get();
        }
        return usuario;
    }

    public Usuario atualizarUsuario(Integer idUsuario, Usuario usuario){
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(idUsuario);
        Usuario auxUsuario = null;
        if(optionalUsuario.isPresent()){
            auxUsuario = optionalUsuario.get();

            usuario.setId(auxUsuario.getId());

            usuarioRepository.save(usuario);
        }
        return usuario;
    }

    public Usuario inserirUsuario(Usuario usuario){
        Usuario objetoUsuario = usuarioRepository.save(usuario);
        return objetoUsuario;
    }

    public void deletarUsuario(Integer idUsuario){
        usuarioRepository.deleteById(idUsuario);
    }
}
