package br.com.ponto.controllers;

import br.com.ponto.dtos.RegistroDetalheDto;
import br.com.ponto.enums.TipoRegistro;
import br.com.ponto.models.Registro;
import br.com.ponto.models.Usuario;
import br.com.ponto.repositories.RegistroRepository;
import br.com.ponto.services.RegistroService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(RegistroController.class)
public class RegistroControllerTest {

    @MockBean
    private RegistroService registroService;

    @MockBean
    private RegistroRepository registroRepository;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;
    Registro registro;
    RegistroDetalheDto registroDetalheDto;
    List<Registro> registros;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setCpf("392.732.638-03");
        usuario.setDataCadastro(LocalDate.of(2020, 07, 06));
        usuario.setEmail("aldermariano@yahoo.com.br");
        usuario.setNomeCompleto("Alder Rienes Mariano");

        registro = new Registro();
        registro.setUsuario(usuario);
        registro.setId(1);
        registro.setTipoRegistro(TipoRegistro.ENTRADA);
        registro.setDataHoraRegistro(LocalDateTime.now());

        registros = new ArrayList<>();
        registros.add(registro);

        registroDetalheDto = new RegistroDetalheDto();
        registroDetalheDto.setRegistros(registros);
        registroDetalheDto.setQuantidadeHorasTrabalhadas(BigDecimal.valueOf(40));

    }

    @Test
    public void testeRegistrarPontoEntrada() throws Exception {
        int id = 1;

        Mockito.when(registroService.registrarPonto(id, TipoRegistro.ENTRADA)).thenReturn(registro);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeRegistro = mapper.writeValueAsString(registro);

        mockMvc.perform(MockMvcRequestBuilders.post("/registros/usuario/{idUsuario}", id).param("tipoRegistro", "ENTRADA")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeRegistro))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testeRegistrarPontoSaida() throws Exception {
        int id = 1;

        Mockito.when(registroService.registrarPonto(id, TipoRegistro.SAIDA)).thenReturn(registro);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeRegistro = mapper.writeValueAsString(registro);

        mockMvc.perform(MockMvcRequestBuilders.post("/registros/usuario/{idUsuario}", id).param("tipoRegistro", "SAIDA")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeRegistro))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testeSelecionarRegistrosPorUsuario() throws Exception{
        Mockito.when(registroService.selecionarRegistrosPorUsuario(Mockito.anyInt())).thenReturn(registroDetalheDto);

        mockMvc.perform(MockMvcRequestBuilders.get("/registros/usuario/{idUsuario}", Mockito.anyInt())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
