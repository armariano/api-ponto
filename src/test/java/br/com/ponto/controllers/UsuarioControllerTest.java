package br.com.ponto.controllers;

import br.com.ponto.models.Usuario;
import br.com.ponto.repositories.UsuarioRepository;
import br.com.ponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setCpf("392.732.638-03");
        usuario.setDataCadastro(LocalDate.of(2020, 07, 06));
        usuario.setEmail("aldermariano@yahoo.com.br");
        usuario.setNomeCompleto("Alder Rienes Mariano");

    }

    @Test
    public void testeSelecionarTodosUsuario() throws Exception {

        Iterable<Usuario> usuariosIterable = new ArrayList<>();
        Mockito.when(usuarioService.selecionarTodosUsuarios()).thenReturn(usuariosIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testeSelecionarUsuarioPorId() throws Exception {
        Mockito.when(usuarioService.selecionarUsuarioPorId(Mockito.anyInt())).thenReturn(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/{id}", Mockito.anyInt())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testeDeletarUsuario() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/usuarios/{id}", Mockito.anyInt())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
