package br.com.ponto.services;

import br.com.ponto.models.Usuario;
import br.com.ponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    Usuario usuario;
    Optional<Usuario> optionalUsuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setCpf("392.732.638-01");
        usuario.setDataCadastro(LocalDate.now());
        usuario.setEmail("aldermariano@yahoo.com.br");
        usuario.setNomeCompleto("Alder Rienes Mariano");

    }

    @Test
    public void testeSelecionarTodosUsuarios(){
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);

        Iterable<Usuario> usuariosIterable = usuarioService.selecionarTodosUsuarios();

        Assertions.assertEquals(usuarios, usuariosIterable);
    }

    @Test
    public void testeSelecionarUsuarioPorId(){
        int id = 1;

        Mockito.when(usuarioRepository.findById(id)).thenReturn(Optional.ofNullable(usuario));

        Usuario usuarioRetorno = usuarioService.selecionarUsuarioPorId(id);

        Assertions.assertEquals(optionalUsuario, null);
    }

    @Test
    public void testeDeletarUsuario() {

        Mockito.when(usuarioRepository.existsById(1)).thenReturn(true);

        usuarioService.deletarUsuario(1);

        Mockito.verify(usuarioRepository, Mockito.times(1)).deleteById(1);

    }

    @Test
    public void testeInserirUsuario(){
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);

        Usuario usuarioObjeto = usuarioService.inserirUsuario(usuario);

        Assertions.assertEquals(usuario, usuarioObjeto);
    }
}
