package br.com.ponto.services;

import br.com.ponto.dtos.RegistroDetalheDto;
import br.com.ponto.enums.TipoRegistro;
import br.com.ponto.models.Registro;
import br.com.ponto.models.Usuario;
import br.com.ponto.repositories.RegistroRepository;
import br.com.ponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestExecutionListeners;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class RegistroServiceTest {

    @MockBean
    private RegistroRepository registroRepository;

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private RegistroService registroService;

    @Autowired
    private UsuarioService usuarioService;

    Usuario usuario;
    Registro registro;
    RegistroDetalheDto registroDetalheDto;
    List<Registro> registros;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setCpf("392.732.638-03");
        usuario.setDataCadastro(LocalDate.of(2020, 07, 06));
        usuario.setEmail("aldermariano@yahoo.com.br");
        usuario.setNomeCompleto("Alder Rienes Mariano");

        registro = new Registro();
        registro.setUsuario(usuario);
        registro.setId(1);
        registro.setTipoRegistro(TipoRegistro.ENTRADA);
        registro.setDataHoraRegistro(LocalDateTime.now());

        registros = new ArrayList<>();
        registros.add(registro);

        registroDetalheDto = new RegistroDetalheDto();
        registroDetalheDto.setRegistros(registros);
        registroDetalheDto.setQuantidadeHorasTrabalhadas(BigDecimal.valueOf(40));

    }
}
